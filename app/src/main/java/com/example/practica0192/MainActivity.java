package com.example.practica0192;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnPulsar;
    private Button btnBorrar;
    private Button btnTerminar;
    private EditText txtNombre;
    private TextView lblSaludar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //relacionar los objetos
        btnPulsar = findViewById(R.id.btnSaludar);
        btnBorrar = findViewById(R.id.btnLimpiar);
        btnTerminar = findViewById(R.id.btnCerrar);
        txtNombre = findViewById(R.id.txtNombre);
        lblSaludar = findViewById(R.id.lblSaludo);

        //Codificar el evento clic del boton
        btnPulsar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validar si nombre está vacío
                if (txtNombre.getText().toString().matches("")) {
                    //Toast - context es para saber sobre dónde está trabajando, en este caso es MainActivity
                    Toast.makeText(MainActivity.this, "Faltó capturar información",
                            Toast.LENGTH_LONG).show();
                } else {
                    String str = "Hola " + txtNombre.getText().toString() + " ¿Cómo estás?";

                    lblSaludar.setText(str.toString());
                }
            }
        });

        //Boton Limpiar
        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validar si nombre está vacío
                if (txtNombre.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "No hay algo que borrar",
                            Toast.LENGTH_LONG).show();
                } else {
                    txtNombre.setText("");
                    lblSaludar.setText(":: ::");
                }
            }
        });

        //Boton Cerrar
        btnTerminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }


        });


    }
}